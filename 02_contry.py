# fetch country trends

from dotenv import load_dotenv
import os
import tweepy
load_dotenv()

auth = tweepy.OAuthHandler(os.environ["api_key"], os.environ["api_secret"])
auth.set_access_token(os.environ["access_token"], os.environ["access_secret"])

api = tweepy.API(auth)

# https://www.woeids.com/
woeid_nk = 23424865
woeid_sk = 23424868

try:
    # fetching trends
    trends = api.get_place_trends(id=woeid_sk)
    print("Twitter trends:")
    for value in trends:
        for trend in value['trends']:
            print(trend['name'])
except:
    print('No social media available')
