# parse user timeline

from dotenv import load_dotenv
import os
import tweepy
load_dotenv()

auth = tweepy.OAuthHandler(os.environ["api_key"], os.environ["api_secret"])
auth.set_access_token(os.environ["access_token"], os.environ["access_secret"])

api = tweepy.API(auth)

try:
    for status in tweepy.Cursor(api.user_timeline,
                                screen_name='@pcantaluppi',
                                tweet_mode="extended").items():
        print(status.created_at, status.full_text)
except:
    print('Error reading timeline')