# follower graph

from dotenv import load_dotenv
from igraph import *
import os
import tweepy
load_dotenv()

auth = tweepy.OAuthHandler(os.environ["api_key"], os.environ["api_secret"])
auth.set_access_token(os.environ["access_token"], os.environ["access_secret"])

api = tweepy.API(auth, wait_on_rate_limit=True)

screen_name = "pcantaluppi"
# screen_name = "britneyspears"
user_id = api.get_user(screen_name=screen_name).id_str

# get followers
ids = []
for page in tweepy.Cursor(api.get_follower_ids, screen_name=screen_name, count=200).pages():
    ids.extend(page)

# array of vertices
vertices = []
for item in ids:
    vertices.append(str(item))

# two-dimensional array of edges
edges = []
for follower_id in vertices:
    edges.append((user_id, str(follower_id)))

# add myself
vertices.append(user_id)

# fetch real name
user_names = []
for user_id in vertices:
    user_names.append(api.get_user(id=user_id).name)

# draw follower graph
g = Graph(directed=True)
g.add_vertices(vertices)
g.add_edges(edges)
g.vs["user_name"] = user_names
style = {"edge_curved": False, "margin": 100,
         "vertex_label": g.vs["user_name"]}
for e in g.es:
    src, targ = e.tuple
    if g.are_connected(targ, src):
        e["color"] = "green" # follow back
        e["arrow_size"] = 0
    else:
        e["color"] = "blue" # follow only
        e["arrow_size"] = 1
plot(g, **style)
